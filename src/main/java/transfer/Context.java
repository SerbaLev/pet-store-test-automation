package transfer;

import dto.response.common.ErrorOrNotificationDTO;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;
import lombok.AllArgsConstructor;

/**
 * Context class is a container for api response.
 * After request execution response information set in context
 * so user can get response, object from response, error or notification object from response, status code
 * and transfer it to different test framework layers
 *
 * @param <T>
 */
@AllArgsConstructor
public class Context<T> {

    private Response response;
    private Class<T> cls;

    public int getResponseStatusCode() {
        return response.getStatusCode();
    }

    public String getResponseAsString() {
        return response.getBody().asString();
    }

    public ErrorOrNotificationDTO getErrorOrNotification() {
        return getExtractor().as(ErrorOrNotificationDTO.class);
    }

    public T getObjectFromResponse() {
        return getExtractor().as(cls);
    }

    private ExtractableResponse<Response> getExtractor() {
        return response.then().log().all().extract();
    }

}
