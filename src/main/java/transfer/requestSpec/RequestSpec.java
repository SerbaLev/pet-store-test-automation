package transfer.requestSpec;

import configuration.Configuration;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import transfer.BaseTransfer;
import transfer.Headers;

import static io.restassured.RestAssured.given;

/**
 * Class contains request specifications that can be used in BaseTransfer class
 *
 * @see BaseTransfer
 */
public class RequestSpec {

    private Configuration configuration = new Configuration();

    public RequestSpecification buildCreateOrUpdateRequest(Object body) {
        return given().spec(getRequestSpecification()).log().all().body(body).when();
    }

    public RequestSpecification getRequestSpecificationMultipart() {
        return new RequestSpecBuilder()
                .setContentType("multipart/form-data")
                .setBaseUri(configuration.getValue(Configuration.Options.BASE_PATH)).build();
    }

    public RequestSpecification getRequestSpecificationUrlEncodedForm() {
        return new RequestSpecBuilder()
                .setConfig(RestAssured.config()
                        .encoderConfig(EncoderConfig.encoderConfig()
                                .encodeContentTypeAs("x-www-form-urlencoded", ContentType.URLENC)))
                .setContentType(ContentType.URLENC.withCharset("UTF-8"))
                .setBaseUri(configuration.getValue(Configuration.Options.BASE_PATH)).build();
    }

    public RequestSpecification getRequestSpecificationWithHeaders() {
        return getBaseRequestSpecBuilder()
                .addHeader(Headers.API_KEY.getValue(), configuration.getValue(Configuration.Options.API_KEY))
                .build();
    }

    public RequestSpecification getRequestSpecification() {
        return getBaseRequestSpecBuilder().build();
    }

    private RequestSpecBuilder getBaseRequestSpecBuilder() {
        return new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri(configuration.getValue(Configuration.Options.BASE_PATH));
    }

}
