package transfer.endpoints.pet;

import dto.request.pet.AddOrUpdatePetRequestDTO;
import dto.response.pet.PetResponseDTO;
import io.restassured.response.Response;
import lombok.Getter;
import transfer.BaseTransfer;
import transfer.Context;

/**
 * Pet Store endpoints created as objects with their own path and methods
 * so they can be built with chain access which represents api structure
 * and makes it easier to work with complex paths
 */
public class Pet {

    private static final String ROUT = "/pet";
    private final BaseTransfer baseTransfer = new BaseTransfer();
    private final String path;
    @Getter
    private final ByPetId byPetId;
    @Getter
    private final FindByStatus findByStatus;

    public Pet(String parentRout) {
        this.path = parentRout + ROUT;
        this.byPetId = new ByPetId(path);
        this.findByStatus = new FindByStatus(path);
    }

    public Context<PetResponseDTO> post(AddOrUpdatePetRequestDTO request) {
        Response response = baseTransfer.post(request, path);
        return new Context<>(response, PetResponseDTO.class);
    }

    public Context<PetResponseDTO> put(AddOrUpdatePetRequestDTO request) {
        Response response = baseTransfer.put(request, path);
        return new Context<>(response, PetResponseDTO.class);
    }

}
