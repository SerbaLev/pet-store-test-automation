package transfer.endpoints.store;

import dto.response.store.InventoryResponseDTO;
import io.restassured.response.Response;
import transfer.BaseTransfer;
import transfer.Context;

/**
 * Pet Store endpoints created as objects with their own path and methods
 * so they can be built with chain access which represents api structure
 * and makes it easier to work with complex paths
 */
public class Inventory {

    private static final String ROUT = "/inventory";
    private final BaseTransfer baseTransfer = new BaseTransfer();
    private final String path;

    public Inventory(String parentRout) {
        this.path = parentRout + ROUT;
    }

    public Context<InventoryResponseDTO> get() {
        Response response = baseTransfer.get(path);
        return new Context<>(response, InventoryResponseDTO.class);
    }

}
