package transfer.endpoints.store;

import dto.request.store.OrderRequestDTO;
import dto.response.store.OrderResponseDTO;
import io.restassured.response.Response;
import lombok.Getter;
import transfer.BaseTransfer;
import transfer.Context;

/**
 * Pet Store endpoints created as objects with their own path and methods
 * so they can be built with chain access which represents api structure
 * and makes it easier to work with complex paths
 */
public class Order {

    private static final String ROUT = "/order";
    private final BaseTransfer baseTransfer = new BaseTransfer();
    private final String path;
    @Getter
    private final ByOrderId byOrderId;

    public Order(String parentRout) {
        this.path = parentRout + ROUT;
        this.byOrderId = new ByOrderId(path);
    }

    public Context<OrderResponseDTO> post(OrderRequestDTO request) {
        Response response = baseTransfer.post(request, path);
        return new Context<>(response, OrderResponseDTO.class);
    }

}
