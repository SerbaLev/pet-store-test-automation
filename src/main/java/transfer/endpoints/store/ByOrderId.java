package transfer.endpoints.store;

import dto.response.common.ErrorOrNotificationDTO;
import dto.response.store.OrderResponseDTO;
import io.restassured.response.Response;
import transfer.BaseTransfer;
import transfer.Context;

/**
 * Pet Store endpoints created as objects with their own path and methods
 * so they can be built with chain access which represents api structure
 * and makes it easier to work with complex paths
 */
public class ByOrderId {

    private static final String ROUT = "/{orderId}";
    private final BaseTransfer baseTransfer = new BaseTransfer();
    private final String path;

    public ByOrderId(String parentRout) {
        this.path = parentRout + ROUT;
    }

    public Context<OrderResponseDTO> get(String id) {
        Response response = baseTransfer.get(id, path);
        return new Context<>(response, OrderResponseDTO.class);
    }

    public Context<ErrorOrNotificationDTO> delete(String id) {
        Response response = baseTransfer.delete(id, path);
        return new Context<>(response, ErrorOrNotificationDTO.class);
    }

}
