package transfer.endpoints.user;

import dto.response.common.ErrorOrNotificationDTO;
import io.restassured.response.Response;
import transfer.BaseTransfer;
import transfer.Context;

/**
 * Pet Store endpoints created as objects with their own path and methods
 * so they can be built with chain access which represents api structure
 * and makes it easier to work with complex paths
 */
public class LogOut {

    private static final String ROUT = "/logout";
    private final BaseTransfer baseTransfer = new BaseTransfer();
    private final String path;

    public LogOut(String parentRout) {
        this.path = parentRout + ROUT;
    }

    public Context<ErrorOrNotificationDTO> get() {
        Response response = baseTransfer.get(path);
        return new Context<>(response, ErrorOrNotificationDTO.class);
    }

}
