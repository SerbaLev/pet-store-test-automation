package transfer.endpoints.user;

import dto.request.user.CreateUserRequestDTO;
import dto.response.common.ErrorOrNotificationDTO;
import dto.response.user.UserResponseDTO;
import io.restassured.response.Response;
import transfer.BaseTransfer;
import transfer.Context;

/**
 * Pet Store endpoints created as objects with their own path and methods
 * so they can be built with chain access which represents api structure
 * and makes it easier to work with complex paths
 */
public class Username {

    private static final String ROUT = "/{username}";
    private final BaseTransfer baseTransfer = new BaseTransfer();
    private final String path;

    public Username(String parentRout) {
        this.path = parentRout + ROUT;
    }

    public Context<UserResponseDTO> get(String username) {
        Response response = baseTransfer.get(username, path);
        return new Context<>(response, UserResponseDTO.class);
    }

    public Context<ErrorOrNotificationDTO> delete(String username) {
        Response response = baseTransfer.delete(username, path);
        return new Context<>(response, ErrorOrNotificationDTO.class);
    }

    public Context<ErrorOrNotificationDTO> put(String username, CreateUserRequestDTO request) {
        Response response = baseTransfer.put(username, request, path);
        return new Context<>(response, ErrorOrNotificationDTO.class);
    }
}
