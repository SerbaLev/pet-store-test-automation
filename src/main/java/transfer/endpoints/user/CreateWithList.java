package transfer.endpoints.user;

import dto.request.user.CreateUserRequestDTO;
import dto.response.common.ErrorOrNotificationDTO;
import io.restassured.response.Response;
import transfer.BaseTransfer;
import transfer.Context;

import java.util.List;

/**
 * Pet Store endpoints created as objects with their own path and methods
 * so they can be built with chain access which represents api structure
 * and makes it easier to work with complex paths
 */
public class CreateWithList {

    private static final String ROUT = "/createWithArray";
    private final BaseTransfer baseTransfer = new BaseTransfer();
    private final String path;

    public CreateWithList(String parentRout) {
        this.path = parentRout + ROUT;
    }

    public Context<ErrorOrNotificationDTO> post(List<CreateUserRequestDTO> request) {
        Response response = baseTransfer.post(request, path);
        return new Context<>(response, ErrorOrNotificationDTO.class);
    }

}
