package transfer.endpoints.user;

import dto.response.common.ErrorOrNotificationDTO;
import io.restassured.response.Response;
import transfer.BaseTransfer;
import transfer.Context;

/**
 * Pet Store endpoints created as objects with their own path and methods
 * so they can be built with chain access which represents api structure
 * and makes it easier to work with complex paths
 */
public class LogIn {

    private static final String ROUT = "/login";
    private final BaseTransfer baseTransfer = new BaseTransfer();
    private final String path;

    public LogIn(String parentRout) {
        this.path = parentRout + ROUT;
    }

    public Context<ErrorOrNotificationDTO> get(String fieldName1, String username, String fieldName2, String password) {
        Response response = baseTransfer.get(fieldName1, username, fieldName2, password, path);
        return new Context<>(response, ErrorOrNotificationDTO.class);
    }
}
