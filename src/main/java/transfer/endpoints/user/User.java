package transfer.endpoints.user;

import dto.request.user.CreateUserRequestDTO;
import dto.response.common.ErrorOrNotificationDTO;
import io.restassured.response.Response;
import lombok.Getter;
import transfer.BaseTransfer;
import transfer.Context;

/**
 * Pet Store endpoints created as objects with their own path and methods
 * so they can be built with chain access which represents api structure
 * and makes it easier to work with complex paths
 */
public class User {

    private static final String ROUT = "/user";
    private final BaseTransfer baseTransfer = new BaseTransfer();
    private final String path;
    @Getter
    private final CreateWithArray createWithArray;
    @Getter
    private final CreateWithList createWithList;
    @Getter
    private final LogIn logIn;
    @Getter
    private final LogOut logOut;
    @Getter
    private final Username username;

    public User(String parentRout) {
        this.path = parentRout + ROUT;
        this.createWithArray = new CreateWithArray(path);
        this.createWithList = new CreateWithList(path);
        this.logIn = new LogIn(path);
        this.logOut = new LogOut(path);
        this.username = new Username(path);
    }

    public Context<ErrorOrNotificationDTO> post(CreateUserRequestDTO request) {
        Response response = baseTransfer.post(request, path);
        return new Context<>(response, ErrorOrNotificationDTO.class);
    }

}
