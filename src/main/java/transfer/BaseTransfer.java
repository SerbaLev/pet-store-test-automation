package transfer;

import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import transfer.requestSpec.RequestSpec;

import java.io.File;
import java.util.List;

import static io.restassured.RestAssured.given;

/**
 * Class contains common requests which can be executed with rest assured
 * Base transfer class is used by endpoints object
 *
 * @see transfer.endpoints
 */
@Slf4j
public class BaseTransfer {

    private RequestSpec requestSpec = new RequestSpec();

    public Response get(String paramName, List<String> params, String path) {
        return given().urlEncodingEnabled(true).spec(requestSpec.getRequestSpecification()).log().all().queryParam(paramName, params).when().get(path);
    }

    public Response get(String id, String path) {
        return given().spec(requestSpec.getRequestSpecification()).log().all().when().get(path, id);
    }

    public Response get(String path) {
        return given().spec(requestSpec.getRequestSpecification()).log().all().when().get(path);
    }

    public Response get(String fieldName1, String username, String fieldName2, String password, String path) {
        return given().urlEncodingEnabled(true)
                .spec(requestSpec.getRequestSpecification()).log().all()
                .queryParam(fieldName1, username).queryParam(fieldName2, password).when().get(path);
    }

    public Response post(Object body, String path) {
        return requestSpec.buildCreateOrUpdateRequest(body).post(path);
    }

    public Response postWithFormStringParams(String id, String formField1, String formValue1, String formField2, String formValue2, String path) {
        return given().log().all().spec(requestSpec.getRequestSpecificationUrlEncodedForm())
                .formParam(formField1, formValue1)
                .formParam(formField2, formValue2)
                .post(path, id);
    }

    public Response postWithFormParams(String id, String formField1, String formValue1,
                                       String formField2, File formValue2, String path) {
        return given().spec(requestSpec.getRequestSpecificationMultipart()).log().all()
                .multiPart(formField1, formValue1)
                .multiPart(formField2, formValue2)
                .post(path, id);
    }

    public Response put(Object body, String path) {
        return requestSpec.buildCreateOrUpdateRequest(body).put(path);
    }

    public Response put(String param, Object body, String path) {
        return requestSpec.buildCreateOrUpdateRequest(body).put(path, param);
    }

    public Response delete(String id, String path) {
        return given().spec(requestSpec.getRequestSpecificationWithHeaders()).log().all().when().delete(path, id);
    }

}
