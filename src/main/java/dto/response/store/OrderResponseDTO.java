package dto.response.store;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Accessors(chain = true, fluent = true)
@EqualsAndHashCode
@ToString
public class OrderResponseDTO {

    private String id;
    private String petId;
    private String quantity;
    private String shipDate;
    private String status;
    private String complete;

}
