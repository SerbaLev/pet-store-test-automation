@StoreNegativeTests

Feature: Access to pet store orders
  As a pet store service consumer
  I want to be able to work with pet store orders
  So that I can place an order for a pet and work with purchase orders


  Scenario: Place an order for purchasing the pet with incorrect input
    Given User provides information for purchasing the pet
      | id    | petId     | quantity | shipDate | status | complete |
      | zsgzs | 26zsgz236 | 2        | gfzgz    | placed | true     |
    When User send "place an order" request
    Then Order request executed with status code 400 in the response

  Scenario: Find an order for purchasing the pet by unknown id
    Given User provides order id
      | id      |
      | 5271613 |
    When User send "find order by id" request
    Then Order request executed with status code 404 in the response
    And Order request executed with correct notification or error in the response
      | code | type  | message         |
      | 404  | error | Order not found |

  Scenario: Find an order for purchasing the pet by unknown id
    Given User provides order id
      | id      |
      | gerghsh |
    When User send "find order by id" request
    Then Order request executed with status code 400 in the response

  Scenario: Delete an order for purchasing the pet by unknown id
    Given User provides order id
      | id    |
      | 12345 |
    When User send "delete order by id" request
    Then Order request executed with status code 404 in the response
    And Order request executed with correct notification or error in the response
      | code | type    | message         |
      | 404  | unknown | Order Not Found |

  Scenario: Delete an order for purchasing the pet by invalid id
    Given User provides order id
      | id    |
      | argga |
    When User send "delete order by id" request
    Then Order request executed with status code 400 in the response
