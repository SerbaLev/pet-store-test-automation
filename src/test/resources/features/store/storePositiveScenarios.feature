@StorePositiveTests

Feature: Access to pet store orders
  As a pet store service consumer
  I want to be able to work with pet store orders
  So that I can place an order for a pet and work with purchase orders


  Scenario: Place an order for purchasing the pet
    Given User provides information for purchasing the pet
      | id    | petId    | quantity | shipDate                     | status | complete |
      | 12345 | 26881236 | 2        | 2020-08-31T11:53:34.205+0000 | placed | true     |
    When User send "place an order" request
    Then Order request executed with status code 200 in the response
    And Order request executed with correct response

  Scenario: Find an order for purchasing the pet by id
    Given User provides information for purchasing the pet
      | id    | petId    | quantity | shipDate                     | status | complete |
      | 12345 | 26881236 | 2        | 2020-08-31T11:53:34.205+0000 | placed | true     |
    And User send "place an order" request
    And Order request executed with status code 200 in the response
    When User provides order id
      | id    |
      | 12345 |
    And User send "find order by id" request
    Then Order request executed with status code 200 in the response
    And Order request executed with correct response

  Scenario: Delete an order for purchasing the pet by id
    Given User provides information for purchasing the pet
      | id    | petId    | quantity | shipDate                     | status | complete |
      | 12345 | 26881236 | 2        | 2020-08-31T11:53:34.205+0000 | placed | true     |
    And User send "place an order" request
    And Order request executed with status code 200 in the response
    When User provides order id
      | id    |
      | 12345 |
    And User send "delete order by id" request
    Then Order request executed with status code 200 in the response
    And Order request executed with correct notification or error in the response
      | code | type    | message |
      | 200  | unknown | 12345   |

  Scenario: Get pet store inventory
    Given User send "get store inventory" request
    Then Order request executed with status code 200 in the response
