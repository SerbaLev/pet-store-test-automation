@UserNegativeTests

Feature: Operations with user
  As a pet store service consumer
  I want to be able to do operations with user
  So that I can create, update, delete users and perform log in, log out operations


  Scenario: Get user by nonexistent username
    Given User provides username
      | username |
      | dhshshs  |
    When User send "get user by username" request
    Then Create users request executed with status code 404 in the response
    And Create users request executed with correct notification or error in the response
      | code | type  | message        |
      | 404  | error | User not found |

  Scenario: Update user by username with nonexistent value
    Given User provides username
      | username               |
      | dz22dfbdzhshrshrshszhz |
    When User send "update user by username" request
    Then Create users request executed with status code 404 in the response
    And Create users request executed with correct notification or error in the response
      | code | type    | message        |
      | 404  | unknown | User not found |

  Scenario: Delete user by username
    Given User provides username
      | username     |
      | qwryhqhhqhwv |
    And User send "delete user by username" request
    Then Create users request executed with status code 404 in the response

  Scenario: Logs user into the system
    Given User provides username and password
      | username             | password                             |
      | B363hehsehse3ehsehsh | qwaejWRJRSWRFYHSSVSgsg454&&errrtty15 |
    When User send "log in" request
    Then Create users request executed with status code 400 in the response
