@UserPositiveTests

Feature: Operations with user
  As a pet store service consumer
  I want to be able to do operations with user
  So that I can create, update, delete users and perform log in, log out operations


  Scenario: Create list of users with array
    Given User provides information about accounts that should be created
      | id   | username | firstName | lastName | email          | password | phone        | userStatus |
      | 5422 | IvanBoo  | Ivan      | Boo      | boo@booble.com | qwerty07 | 209548273485 | 1          |
      | 4562 | VasilDoo | Vasil     | Doo      | doo@booble.com | qwerty08 | 345548273485 | 0          |
    When User send "create users with array" request
    Then Create users request executed with status code 200 in the response
    And Create users request executed with correct notification or error in the response
      | code | type    | message |
      | 200  | unknown | ok      |

  Scenario: Create user
    Given User provides information about account that should be created or updated
      | id   | username | firstName | lastName | email           | password | phone        | userStatus |
      | 8888 | IvanBoo  | Ivan      | Boo      | boo2@booble.com | qwerty15 | 209548273485 | 2          |
    When User send "create user" request
    Then Create users request executed with status code 200 in the response
    And Create users request executed with correct notification or error in the response
      | code | type    | message |
      | 200  | unknown | 8888    |

  Scenario: Create list of users with list
    Given User provides information about accounts that should be created
      | id   | username | firstName | lastName | email          | password | phone        | userStatus |
      | 5422 | IvanBoo  | Ivan      | Boo      | boo@booble.com | qwerty07 | 209548273485 | 1          |
      | 4562 | VasilDoo | Vasil     | Doo      | doo@booble.com | qwerty08 | 345548273485 | 0          |
    When User send "create users with list" request
    Then Create users request executed with status code 200 in the response
    And Create users request executed with correct notification or error in the response
      | code | type    | message |
      | 200  | unknown | ok      |

  Scenario: Get user by username
    Given User provides information about accounts that should be created
      | id         | username | firstName | lastName | email          | password | phone        | userStatus |
      | 5463525222 | AlexBoo  | Ivan      | Boo      | boo@booble.com | qwerty07 | 209548273485 | 1          |
    And User send "create users with list" request
    And Create users request executed with status code 200 in the response
    When User provides username
      | username |
      | AlexBoo  |
    And User send "get user by username" request
    Then Create users request executed with status code 200 in the response
    And Create users request executed with correct response

  Scenario: Delete user by username
    Given User provides information about accounts that should be created
      | id   | username | firstName | lastName | email          | password | phone        | userStatus |
      | 5422 | IvanBoo  | Ivan      | Boo      | boo@booble.com | qwerty07 | 209548273485 | 1          |
    And User send "create users with list" request
    And Create users request executed with status code 200 in the response
    When User provides username
      | username |
      | IvanBoo  |
    And User send "delete user by username" request
    Then Create users request executed with status code 200 in the response
    And Create users request executed with correct notification or error in the response
      | code | type    | message |
      | 200  | unknown | IvanBoo |

  Scenario: Update user by username
    Given User provides information about accounts that should be created
      | id     | username  | firstName | lastName | email          | password | phone        | userStatus |
      | 525112 | AndrewBoo | Ivan      | Boo      | boo@booble.com | qwerty07 | 209548273485 | 1          |
    And User send "create users with list" request
    And Create users request executed with status code 200 in the response
    When User provides information about account that should be created or updated
      | id   | username | firstName | lastName | email           | password | phone        | userStatus |
      | 1376 | IvanBoo  | Ivan      | Boo      | boo2@booble.com | qwerty15 | 209548273485 | 2          |
    And User provides username
      | username |
      | IvanBoo  |
    And User send "update user by username" request
    Then Create users request executed with status code 200 in the response
    And Create users request executed with correct notification or error in the response
      | code | type    | message |
      | 200  | unknown | 1376    |

  Scenario: Logs user into the system
    Given User provides information about account that should be created or updated
      | id   | username | firstName | lastName | email           | password | phone        | userStatus |
      | 8888 | IvanBoo  | Ivan      | Boo      | boo2@booble.com | qwerty15 | 209548273485 | 2          |
    And User send "create user" request
    And Create users request executed with status code 200 in the response
    And Create users request executed with correct notification or error in the response
      | code | type    | message |
      | 200  | unknown | 8888    |
    When User provides username and password
      | username | password |
      | IvanBoo  | qwerty15 |
    And User send "log in" request
    Then Create users request executed with status code 200 in the response
    And Create users request executed with correct notification or error and contains values
      | code | type    | message                |
      | 200  | unknown | logged in user session |

  Scenario: Logs out from the system
    Given User send "log out" request
    Then Create users request executed with status code 200 in the response
    And Create users request executed with correct notification or error in the response
      | code | type    | message |
      | 200  | unknown | ok      |