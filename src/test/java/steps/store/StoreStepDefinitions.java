package steps.store;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import net.thucydides.core.annotations.Steps;
import stepsExecutor.store.StoreStepsExecutor;

import java.util.Map;

/**
 * Steps definition for tests related to store endpoint in Pet Store api
 */
public class StoreStepDefinitions {

    @Steps
    StoreStepsExecutor stepsExecutor;

    @Given("User provides information for purchasing the pet$")
    public void userProvidesInformationForPurchasingThePet(DataTable table) {
        Map<String, String> data = table.asMaps().get(0);
        stepsExecutor.orderRequest().id(data.get("id"))
                .petId(data.get("petId"))
                .quantity(data.get("quantity"))
                .shipDate(data.get("shipDate"))
                .status(data.get("status"))
                .complete(data.get("complete"));
    }

    @When("User send \"place an order\" request")
    public void userSendPlaceAnOrderRequest() {
        stepsExecutor.performPlaceAnOrderRequest(stepsExecutor.orderRequest());
    }

    @When("User provides order id$")
    public void userProvidesOrderId(DataTable table) {
        Map<String, String> data = table.asMaps().get(0);
        stepsExecutor.orderId(data.get("id"));
    }

    @When("User send \"find order by id\" request")
    public void userSendFindOrderByIdRequest() {
        stepsExecutor.performFindOrderByIdRequest(stepsExecutor.orderId());
    }

    @When("User send \"delete order by id\" request")
    public void userSendDeleteOrderByIdRequest() {
        stepsExecutor.performDeleteOrderByIdRequest(stepsExecutor.orderId());
    }

    @When("User send \"get store inventory\" request")
    public void userSendGetStoreInventoryRequest() {
        stepsExecutor.performGetStoreInventoryRequest();
    }

    @Then("Order request executed with correct response")
    public void orderRequestExecutedWithCorrectResponse() {
        stepsExecutor.expectedOrderResponse()
                .id(stepsExecutor.orderRequest().id())
                .petId(stepsExecutor.orderRequest().petId())
                .quantity(stepsExecutor.orderRequest().quantity())
                .complete(stepsExecutor.orderRequest().complete())
                .shipDate(stepsExecutor.orderRequest().shipDate())
                .status(stepsExecutor.orderRequest().status());
        stepsExecutor.verifyBodyOfTheResponse(stepsExecutor.actualOrderResponse(), stepsExecutor.expectedOrderResponse());
    }

    @Then("Order request executed with status code {int} in the response")
    public void requestExecutedWithCorrectStatusCode(int statusCode) {
        stepsExecutor.expectedStatusCode(statusCode);
        stepsExecutor.verifyStatusCode(stepsExecutor.actualStatusCode(), stepsExecutor.expectedStatusCode());
    }

    @Then("Order request executed with correct notification or error in the response$")
    public void verifyCorrectResponseNotificationOrError(DataTable table) {
        Map<String, String> data = table.asMaps().get(0);
        stepsExecutor.expectedErrorOrNotificationResponse()
                .code(data.get("code"))
                .message(data.get("message"))
                .type(data.get("type"));
        stepsExecutor.verifyBodyOfTheResponse(stepsExecutor.actualErrorOrNotificationResponse(), stepsExecutor.expectedErrorOrNotificationResponse());
    }

}
