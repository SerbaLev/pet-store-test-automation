package steps.user;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import net.thucydides.core.annotations.Steps;
import stepsExecutor.user.UserStepExecutor;

import java.util.List;
import java.util.Map;

/**
 * Steps definition for tests related to user endpoint in Pet Store api
 */
public class UserStepDefinitions {

    @Steps
    UserStepExecutor stepsExecutor;

    @Given("User provides information about accounts that should be created$")
    public void provideAccountCreationInfo(DataTable table) {
        List<Map<String, String>> data = table.asMaps();
        data.forEach(map -> {
            submitValuesInRequest(map);
            stepsExecutor.createListOfUsersRequest().add(stepsExecutor.createUserRequest());
        });
    }

    @When("User provides information about account that should be created or updated$")
    public void provideAccountUpdateInfo(DataTable table) {
        Map<String, String> data = table.asMaps().get(0);
        submitValuesInRequest(data);
    }

    @When("User provides username and password")
    public void userProvidesUsernameAndPassword(DataTable table) {
        Map<String, String> data = table.asMaps().get(0);
        stepsExecutor.username(data.get("username"));
        stepsExecutor.password(data.get("password"));
    }

    @When("User send \"create users with array\" request")
    public void userSendCreateUsersArrayRequest() {
        stepsExecutor.performCreateUsersArrayRequest(stepsExecutor.createListOfUsersRequest());
    }

    @When("User send \"create user\" request")
    public void userSendCreateUserRequest() {
        stepsExecutor.performCreateUserRequest(stepsExecutor.createUserRequest());
    }

    @When("User send \"log in\" request")
    public void userSendLogInRequest() {
        stepsExecutor.performLogInRequest("username", stepsExecutor.username(), "password", stepsExecutor.password());
    }

    @When("User send \"log out\" request")
    public void userSendLogOutRequest() {
        stepsExecutor.performLogOutRequest();
    }

    @When("User send \"create users with list\" request")
    public void userSendCreateUsersListRequest() {
        stepsExecutor.performCreateUsersListRequest(stepsExecutor.createListOfUsersRequest());
    }

    @When("User provides username$")
    public void userProvidesUsername(DataTable table) {
        Map<String, String> data = table.asMaps().get(0);
        stepsExecutor.username(data.get("username"));
    }

    @When("User send \"get user by username\" request")
    public void userSendGetUserByUsernameRequest() {
        stepsExecutor.performGetUserByUsernameRequest(stepsExecutor.username());
    }

    @When("User send \"update user by username\" request")
    public void userSendUpdateUserByUsernameRequest() {
        stepsExecutor.performUpdateUserByUsernameRequest(stepsExecutor.username(), stepsExecutor.createUserRequest());
    }

    @When("User send \"delete user by username\" request")
    public void userSendDeleteUserByUsernameRequest() {
        stepsExecutor.performDeleteUserByUsernameRequest(stepsExecutor.username());
    }

    @Then("Create users request executed with status code {int} in the response")
    public void requestExecutedWithCorrectStatusCode(int statusCode) {
        stepsExecutor.expectedStatusCode(statusCode);
        stepsExecutor.verifyStatusCode(stepsExecutor.actualStatusCode(), stepsExecutor.expectedStatusCode());
    }

    @Then("Create users request executed with correct notification or error in the response")
    public void verifyCorrectResponseNotificationOrError(DataTable table) {
        Map<String, String> data = table.asMaps().get(0);
        stepsExecutor.expectedErrorOrNotificationResponse()
                .code(data.get("code"))
                .message(data.get("message"))
                .type(data.get("type"));
        stepsExecutor.verifyBodyOfTheResponse(stepsExecutor.actualErrorOrNotificationResponse(), stepsExecutor.expectedErrorOrNotificationResponse());
    }

    @Then("Create users request executed with correct notification or error and contains values")
    public void verifyCorrectResponseNotificationOrErrorContainsValues(DataTable table) {
        Map<String, String> data = table.asMaps().get(0);
        stepsExecutor.expectedErrorOrNotificationResponse()
                .code(data.get("code"))
                .message(data.get("message"))
                .type(data.get("type"));
        stepsExecutor.verifyBodyOfTheResponseContainsValues(stepsExecutor.actualErrorOrNotificationResponse(), stepsExecutor.expectedErrorOrNotificationResponse());
    }

    @Then("Create users request executed with correct response")
    public void createUsersRequestExecutedWithCorrectResponse() {
        stepsExecutor.expectedCreateUserResponse()
                .id(stepsExecutor.createUserRequest().id())
                .username(stepsExecutor.createUserRequest().username())
                .firstName(stepsExecutor.createUserRequest().firstName())
                .lastName(stepsExecutor.createUserRequest().lastName())
                .email(stepsExecutor.createUserRequest().email())
                .password(stepsExecutor.createUserRequest().password())
                .phone(stepsExecutor.createUserRequest().phone())
                .userStatus(stepsExecutor.createUserRequest().userStatus());
        stepsExecutor.verifyBodyOfTheResponse(stepsExecutor.actualCreateUserResponse(), stepsExecutor.expectedCreateUserResponse());
    }

    private void submitValuesInRequest(Map<String, String> map) {
        stepsExecutor.createUserRequest()
                .email(map.get("email"))
                .id(map.get("id"))
                .username(map.get("username"))
                .firstName(map.get("firstName"))
                .lastName(map.get("lastName"))
                .phone(map.get("phone"))
                .password(map.get("password"))
                .userStatus(map.get("userStatus"));
    }

}
