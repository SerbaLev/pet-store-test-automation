package runner;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

/**
 * Configuration class. Makes it possible to run tests with
 * CucumberWithSerenity runner. Can be configured by adding options inside
 * the CucumberOptions annotation
 */
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features", glue = "steps")
public class CucumberWithSerenityRunner {

}