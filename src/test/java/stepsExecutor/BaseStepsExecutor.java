package stepsExecutor;

import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

/**
 * Class contains common methods for steps execution
 * BaseStepsExecutor is used by steps definition layer for Pet Store Api
 *
 * @see steps
 */
public class BaseStepsExecutor {

    @Step("Verify that response contains correct body")
    public <T> void verifyBodyOfTheResponse(T actualResponse, T expectedResponse) {
        assertThat("Actual response does not match the expected one", actualResponse, is(equalTo(expectedResponse)));
    }

    @Step("Verify that status code is correct")
    public void verifyStatusCode(int actualCode, int expectedCode) {
        assertThat("Status codes do not match. Expected: " + expectedCode + " but was: " + actualCode,
                actualCode, is(equalTo(expectedCode)));
    }

}
