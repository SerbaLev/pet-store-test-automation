package stepsExecutor.store;

import dto.request.store.OrderRequestDTO;
import dto.response.common.ErrorOrNotificationDTO;
import dto.response.store.InventoryResponseDTO;
import dto.response.store.OrderResponseDTO;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import net.thucydides.core.annotations.Step;
import stepsExecutor.BaseStepsExecutor;
import transfer.Context;
import transfer.endpoints.PetStoreRestApi;
import transfer.endpoints.store.ByOrderId;
import transfer.endpoints.store.Inventory;
import transfer.endpoints.store.Order;

/**
 * Class contains methods for steps execution
 * StoreStepsExecutor is used by steps definition layer for Store endpoint in Pet Store Api
 *
 * @see steps.store.StoreStepDefinitions
 */
@Slf4j
@Getter
@Setter
@Accessors(chain = true, fluent = true)
@EqualsAndHashCode(callSuper = true)
@ToString
public class StoreStepsExecutor extends BaseStepsExecutor {

    private final Order orderEndpoint = new PetStoreRestApi().getStore().getOrder();
    private final ByOrderId byOrderIdEndpoint = new PetStoreRestApi().getStore().getOrder().getByOrderId();
    private final Inventory inventoryEndpoint = new PetStoreRestApi().getStore().getInventory();

    private OrderRequestDTO orderRequest = new OrderRequestDTO();
    private OrderResponseDTO actualOrderResponse = new OrderResponseDTO();
    private OrderResponseDTO expectedOrderResponse = new OrderResponseDTO();
    private InventoryResponseDTO actualInventoryResponse = new InventoryResponseDTO();
    private ErrorOrNotificationDTO expectedErrorOrNotificationResponse = new ErrorOrNotificationDTO();
    private ErrorOrNotificationDTO actualErrorOrNotificationResponse = new ErrorOrNotificationDTO();
    private Integer actualStatusCode;
    private Integer expectedStatusCode;
    private String orderId;

    @Step("Perform POST request on order endpoint with provided data in request body section")
    public void performPlaceAnOrderRequest(OrderRequestDTO request) {
        Context<OrderResponseDTO> context = orderEndpoint.post(request);
        actualOrderResponse = context.getObjectFromResponse();
        actualStatusCode = context.getResponseStatusCode();
    }

    @Step("Perform GET request on order endpoint with id in path")
    public void performFindOrderByIdRequest(String id) {
        Context<OrderResponseDTO> context = byOrderIdEndpoint.get(id);
        actualOrderResponse = context.getObjectFromResponse();
        actualErrorOrNotificationResponse = context.getErrorOrNotification();
        actualStatusCode = context.getResponseStatusCode();
    }

    @Step("Perform DELETE request on order endpoint with id in path")
    public void performDeleteOrderByIdRequest(String id) {
        Context<ErrorOrNotificationDTO> context = byOrderIdEndpoint.delete(id);
        actualErrorOrNotificationResponse = context.getObjectFromResponse();
        actualStatusCode = context.getResponseStatusCode();
    }

    @Step("Perform GET request on store inventory endpoint with id in path")
    public void performGetStoreInventoryRequest() {
        Context<InventoryResponseDTO> context = inventoryEndpoint.get();
        actualStatusCode = context.getResponseStatusCode();
    }

}
