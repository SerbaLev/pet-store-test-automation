package stepsExecutor.user;

import dto.request.user.CreateUserRequestDTO;
import dto.response.common.ErrorOrNotificationDTO;
import dto.response.user.UserResponseDTO;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import net.thucydides.core.annotations.Step;
import stepsExecutor.BaseStepsExecutor;
import transfer.Context;
import transfer.endpoints.PetStoreRestApi;
import transfer.endpoints.user.CreateWithArray;
import transfer.endpoints.user.CreateWithList;
import transfer.endpoints.user.LogIn;
import transfer.endpoints.user.LogOut;
import transfer.endpoints.user.User;
import transfer.endpoints.user.Username;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

/**
 * Class contains methods for steps execution
 * UserStepsExecutor is used by steps definition layer for User endpoint in Pet Store Api
 *
 * @see steps.user.UserStepDefinitions
 */
@Slf4j
@Getter
@Setter
@Accessors(chain = true, fluent = true)
@EqualsAndHashCode(callSuper = true)
@ToString
public class UserStepExecutor extends BaseStepsExecutor {

    private final CreateWithArray createWithArrayEndpoint = new PetStoreRestApi().getUser().getCreateWithArray();
    private final CreateWithList createWithListEndpoint = new PetStoreRestApi().getUser().getCreateWithList();
    private final Username usernameEndpoint = new PetStoreRestApi().getUser().getUsername();
    private final User userEndpoint = new PetStoreRestApi().getUser();
    private final LogIn logInEndpoint = new PetStoreRestApi().getUser().getLogIn();
    private final LogOut logOutEndpoint = new PetStoreRestApi().getUser().getLogOut();

    private CreateUserRequestDTO createUserRequest = new CreateUserRequestDTO();
    private List<CreateUserRequestDTO> createListOfUsersRequest = new ArrayList<>();
    private ErrorOrNotificationDTO actualErrorOrNotificationResponse = new ErrorOrNotificationDTO();
    private ErrorOrNotificationDTO expectedErrorOrNotificationResponse = new ErrorOrNotificationDTO();
    private UserResponseDTO actualCreateUserResponse = new UserResponseDTO();
    private UserResponseDTO expectedCreateUserResponse = new UserResponseDTO();

    private String username;
    private String password;
    private Integer actualStatusCode;
    private Integer expectedStatusCode;

    @Step("Perform POST request with array of users in body")
    public void performCreateUsersArrayRequest(List<CreateUserRequestDTO> request) {
        Context<ErrorOrNotificationDTO> context = createWithArrayEndpoint.post(request);
        actualErrorOrNotificationResponse = context.getObjectFromResponse();
        actualStatusCode = context.getResponseStatusCode();
    }

    @Step("Perform POST request with list of users in body")
    public void performCreateUsersListRequest(List<CreateUserRequestDTO> request) {
        Context<ErrorOrNotificationDTO> context = createWithListEndpoint.post(request);
        actualErrorOrNotificationResponse = context.getObjectFromResponse();
        actualStatusCode = context.getResponseStatusCode();
    }

    @Step("Perform POST request with user info in body")
    public void performCreateUserRequest(CreateUserRequestDTO request) {
        Context<ErrorOrNotificationDTO> context = userEndpoint.post(request);
        actualErrorOrNotificationResponse = context.getObjectFromResponse();
        actualStatusCode = context.getResponseStatusCode();
    }

    @Step("Perform PUT request with user updates in body and username in path")
    public void performUpdateUserByUsernameRequest(String username, CreateUserRequestDTO userRequest) {
        Context<ErrorOrNotificationDTO> context = usernameEndpoint.put(username, userRequest);
        actualErrorOrNotificationResponse = context.getObjectFromResponse();
        actualStatusCode = context.getResponseStatusCode();
    }

    @Step("Perform GET request with username it path param")
    public void performGetUserByUsernameRequest(String username) {
        Context<UserResponseDTO> context = usernameEndpoint.get(username);
        actualCreateUserResponse = context.getObjectFromResponse();
        actualErrorOrNotificationResponse = context.getErrorOrNotification();
        actualStatusCode = context.getResponseStatusCode();
    }

    @Step("Perform DELETE request with username it path param")
    public void performDeleteUserByUsernameRequest(String username) {
        Context<ErrorOrNotificationDTO> context = usernameEndpoint.delete(username);
        if (!context.getResponseAsString().isEmpty()) {
            actualErrorOrNotificationResponse = context.getObjectFromResponse();
        }
        actualStatusCode = context.getResponseStatusCode();
    }

    @Step("Perform GET request with username and password in path param")
    public void performLogInRequest(String fieldName1, String username, String fieldName2, String password) {
        Context<ErrorOrNotificationDTO> context = logInEndpoint.get(fieldName1, username, fieldName2, password);
        actualErrorOrNotificationResponse = context.getObjectFromResponse();
        actualStatusCode = context.getResponseStatusCode();
    }

    @Step("Perform GET request without params to log out from the system")
    public void performLogOutRequest() {
        Context<ErrorOrNotificationDTO> context = logOutEndpoint.get();
        actualErrorOrNotificationResponse = context.getObjectFromResponse();
        actualStatusCode = context.getResponseStatusCode();
    }

    @Step("Verify that response contains correct values")
    public void verifyBodyOfTheResponseContainsValues(ErrorOrNotificationDTO actualErrorOrNotificationResponse, ErrorOrNotificationDTO expectedErrorOrNotificationResponse) {
        assertThat("Status codes in response do not match",
                actualErrorOrNotificationResponse.code(), is(equalTo(expectedErrorOrNotificationResponse.code())));
        assertThat("Types in response do not match",
                actualErrorOrNotificationResponse.type(), is(equalTo(expectedErrorOrNotificationResponse.type())));
        assertThat("Actual message does not contain correct text",
                actualErrorOrNotificationResponse.message().contains(expectedErrorOrNotificationResponse.message()), is(true));

    }
}
