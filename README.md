**Test automation framework for testing Pet Store API** https://petstore.swagger.io/

**Technologies used**
1) RestAssured
https://github.com/rest-assured/rest-assured/wiki/GettingStarted
2) Cucumber
https://docs.cucumber.io/
3) Serenity BDD
http://thucydides.info/docs/serenity-staging/

**Build tool**
Maven

**Test automation project structure**

_Scenarios layer_
- Contains scenarios in feature files (*.feature) written with Gherkin language syntax

_Step Definitions layer_
- Contains mapping for Gherkin expressions

_Step Executor layes_
- Contains methods for steps execution

_Transfer layer_
- Contains classes and methods for working with rest api requests
- Contains objects for each endpoint so you can build paths according to api structure

_Data storage_
- For data storage and transport DTO`s are used
- Response from API saves into Context class

_Runner_
- Contains runner classes which can be configured for tests execution with different runners

**How to run**
1) Clone repository
2) To run with JUnit create configuration:
    - class: runner.CucumberWithSerenityRunner or you own created runner class
    - VM options: -ea -Dcucumber.options="--tags @tag"
3) To run with Maven execute command:
    - mvn clean verify "-Dcucumber.options=--tags @tag"
4) Report can be found here: target/site/serenity/index.html
5) For test execution inside gitlab pipeline please configure .gitlab-ci.yml file in root directory
    
         
    
 
    


